import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  components: MyComponent[] = [
    {
      icon: 'american-football',
      name: 'Action-sheet',
      redirectTo: '/action-sheet',
    },
    {
      icon: 'list',
      name: 'List',
      redirectTo: '/list',
    },
  ];

  constructor() {
    console.log(this.components[0]);
  }

  ngOnInit() {}
}

interface MyComponent {
  icon: string;
  name: string;
  redirectTo: string;
}
